######################################################################################
# 2016/10/17 - PsychOS 2.6.1 - 64-bit version, netdiscover, Mendeley, and ClamTk fix #
######################################################################################

*Add vlock to PsychOS

*I was going to add netdiscover from source code but, it was way too buggy.
	*It will not terminate with htop and internet stops working
	*May have to use nmap with some options to use MAC addresses

*Removed applications in PsychOS 64-bit version:
	- Scribus
	- Smartgit
	- Giggle
	- Rambox; left the icon for it in /usr/share/pixmaps/
	- MakeHuman

*Update Mendeley by copy and paste contents of expanded generic Linux folder to /opt/mendeleydesktop/

*Fixed ClamTk for Faba, or at least temporarily
