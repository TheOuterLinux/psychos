###################################################################################################################
# PsychOS 08/21/2016 - PsychOS 2.5.6 - Removing the secure file remove feature and bottom panel missing launchers #
###################################################################################################################

* Removing the secure file remove feature from the menu; it's not working 
  and I don't know why; it is too much of a risk to keep.
  
* Bottom panel still missing launchers

* WaveSurfer is still missing in the menu
