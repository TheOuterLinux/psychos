######################################
# 2018/12/22 - PsychOS 3.0.0 - Elive #
######################################

*Booted-up Elive 3.0.3 and... I'm a little impressed so far. It's fun; some of the window effects that used to be popular back in the day (~Ubuntu 10.04) are there and there's even a "voice-over" of sorts when things are "active" instead of being a system sound.

Synaptic Package Manager repos
---------------
deb http://repo.wheezy.elive.elivecd.org wheezy main elive efl games ports fixes drivers multimedia non-free
deb http://repo.wheezy.debian.elivecd.org/debian wheezy main contrib non-free
deb http://repo.wheezy.debian-security.elivecd.org/debian-security wheezy/updates main contrib non-free
deb http://repo.wheezy.debian.elivecd.org/debian wheezy-backports main contrib non-free
[Disabled] deb http://www.debian-multimedia.org/ wheezy main non-free
deb http://repo.wheezy.deb-multimedia.elivecd.org/ whezzy main non-free

htop from Live:
-------
296MB RAM, but might be 194MB; jumps at random
86 tasks
Load avg 0.45 0.41 0.50

Applications to note:
----------
gnome-search-tool
RawTherapee
Luminance HDR
Darktable
Nicotine

Thunar actions:
-------
Convert to OGG
    /usr/libexec/thunar/audio-converter/audio-converter-ogg %F
Convert to MP3
    /usr/libexec/thunar/audio-converter/audio-converter-mp3 %F
Resize image
    /usr/libexec/thunar/image-resizer/resizer %F
Directory Statistics
    baobab %f
Download Subtitles
    periscope -force %F
Download Subtitles in English
    periscope -force -l en %F
Shrink PDF Documents
    pdf-shrink %F
