######################################
# PsychOS 09/23/2016 - PsychOS 2.6.0 #
######################################

* GIMP plugins and scripts added

* Adding Blender addons soon; waiting for 2.78 to come out (folder name issues).
      - I'll probably have to add the download to /opt/ folder because the
        repo Blender comes from hasn't updated since 2.76
           [] Make sure to remove older version and add symbolic
              link to /usr/bin/ folder
              
* Found Kodi's addons folder in /usr/share/kodi/

* Used Brasero to create an ISO from a burned copy of PsychOS. Both
  sha1sum's of the DVD and downloaded ISO are the same.

* Can both Ubuntu/Debian (.deb) and RPM package systems be used if folders
  merged?

* Install pymol and add icon to menu
