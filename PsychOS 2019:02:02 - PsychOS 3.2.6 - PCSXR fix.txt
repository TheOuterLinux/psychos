##########################################
# 2019/02/02 - PsychOS 3.2.6 - PCSXR fix #
##########################################

No need to write a script for PCSXR; I tar.gz the xvideo library file, which leaves
opengl as the only option.