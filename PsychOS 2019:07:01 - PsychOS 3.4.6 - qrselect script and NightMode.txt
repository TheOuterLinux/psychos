##############################################################
# 2019/07/01 - PsychOS 3.4.6 - qrselect script and NightMode #
##############################################################

Added a script called 'qrselect' for displaying QR code based on selected
text.

Finally found a way to adjust screen gamma so I can have a red screen for
things like astronomy.

    xgamma -gamma 1 --> Adjust all rgb values
    xgamma -rgamma 1 -ggamma 0.5 -bgamma 0.5 --> red tinted screen
    
I do plan on figuring out how to do the same but for TTY/console, or 
at least try.
