###############################################################
# 2019/06/09 - PsychOS 3.4.6 - Opening an image with PictView #
###############################################################

Use the following in Thunar Custom Actions to open an older image format
with a DOS program called PictView with DOSBox:

dir=%d; file=%n; fbname="$(basename "${file}" | cut -d . -f1 | sed 's/ //g')"; fext="$(echo "${file}" | awk -F . '{print $NF}')"; if [ "$(echo ${fbname} | wc -c)" -gt 9 ]; then file="$(echo ${fbname} | cut -c1-6)" && file="${file}~1.$fext";fi && dosbox -securemode -exit -c "mount c /home/theouterlinux/DOS/Graphics/pictview" -c "mount d $(readlink -f ${dir})" -c "c:\\pictview.exe d:\\${file}"
