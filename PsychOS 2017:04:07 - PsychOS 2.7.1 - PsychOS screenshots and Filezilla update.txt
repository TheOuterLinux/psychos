#########################################################################
# 2017/04/07 - PsychOS 2.7.1 - PsychOS screenshots and Filezilla update #
#########################################################################

*Added screenshots to theouterlinux.com/psychos; I though I had before, but apparently not.

*Added a way to make sure Filezilla via Upgrade script stays the latest version, or at least for now.
