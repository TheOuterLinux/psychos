##############################################################################
# 2019/02/15 - PsychOS 3.3.3 - Choosing -vo=drm output and raw GitLab files #
##############################################################################

mplayer -vo=fbdev2 /path/to/file

mpv -drm-connector=help | grep '(connected)' | sed 's/(connected)//g' | tail -n 1
mpv -vo drm -drm-connector "above result" /path/to/file

Just figured out a way to render raw html text files from GitLab:
    curl -s https://gitlab.com/.../.../raw/master/foo.html | w3m -T text/html

This way, I can create documentation, that are dynamic without having to hope anyone
takes the time to git pull or create an entire .io address for.

Maybe I should add launchers for 'LibreOffice', 'gpick -p', terminal, etc. to the
panel?