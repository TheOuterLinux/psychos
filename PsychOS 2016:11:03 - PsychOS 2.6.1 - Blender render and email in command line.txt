#########################################################################
# 2016/11/03 - PsychOS 2.6.1 - Blender render and email in command line #
#########################################################################

*Figured a way to render .blend files in command line, but there are so many options that it may need to have its own submenu.

*Wonder if there's a way to use cron to create notifications or update weather and mail info or how many is in a box every 10 minutes or so?

*Can a [???] be written for mc?

*Probably going to have to create more dialog stuff for email to be easier. Need to figure out how to store accounts. Could use bcrypt to protect account info; better than nothing.

---------------------------------------------------------
##############################
# ./email-client Rough Draft #
##############################

--[Send E-mail]--
1. To
2. From
3. Subject
4. Message

Information from $trings are echoed here

#################################################
# 1. To						#
# 2. From					#
# 3. Subject					#
# 4. Message					#
# 5. Attachment					#
# -----[E-mail]------			#
#						#
# To: example@gmail.com				#
# From: foo@gmail.com				#
# Subject: This is a subject			#
# Message: Blah blah blah.			#
# Attachment: ~/Documents/example.tar.gz	#
#################################################
