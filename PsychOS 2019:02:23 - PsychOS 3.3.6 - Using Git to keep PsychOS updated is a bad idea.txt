################################################################################
# 2019/02/23 - PsychOS 3.3.6 - Using Git to keep PsychOS updated is a bad idea #
################################################################################

I just spent the last several hours trying to figure out how to utilize GitLab for
keeping PsychOS up-to-date script and file corrections-wise without having to build
a snapshot for every little thing. I have to remember to 'chmod +x' everything and
even then, I still haven't gotten some .desktops to work.

Sooo... I will not be using Git to keep parts of PsychOS updated. After playing
around with a setup, there were too many problems with 'git pull' and it has a large
.pack file inside of .git.