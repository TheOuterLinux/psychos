#######################################################
# 2017/03/24 - PsychOS 2.7.0 - nmcli menu for climenu #
#######################################################

*nmcli seems to be very simple to use
	*nmcli -p c -> list connection profile
	*nmcli d wifi c "Connection Name" password "Password" name "SSID" -> Setup connection
	*sudo iw d wlan0 scan | grep SSID -> scan for wifi
	*nmcli -p d -> list devices (eth0, wlan0, etc.)
	*nmcli r all off -> turns off all radio/wifi

*Adding nmcli to climenu for easier network connecting
	*Menu items:
		1. List devices
		2. List connection profiles
		3. Scan for wireless routers
		4. Setup new wireless connection
		5. Turn all wifi on/off
		6. Connection information
