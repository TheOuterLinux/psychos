#########################################################################
# 2019/01/11 - PsychOS 3.0.9 - MIDI playback issues, Palemoon hates     #
#                              Sketchfab on older systems, and possible #
#                              extras                                   #
#########################################################################

Hide non-sudo Synaptic and rename the sudo version.
Do the same for GParted.

Quod Libet will not play MIDI out-of-the-box and I will need to disable
"Save rating and play counts" option in settings because I don't
understand why there's an email address.

Apparently, 'pmidi' is used to port-forward a MIDI file to another program
like Yoshimi or ZynAddSubFX.

Going to Sketchfab.com did something I haven't seen in a few years and
that's a black screen crash, which I commonly found while running actual
Google Chrome on Ubuntu-based 14.04 32-bit systems.

Possible Extras
--------
**Try to list only ones that can't be installed with 'apt'**

Twine
RenPy
SuperTux 2
Frets on Fire
RetroArch !![2019/02/15] - Disabled repo added just for this
SuperTuxKart
RuneScape !![2019/02/15] - Disabled repo added just for this
OpenToonz
Guitarix Rakarrack
OpenEMS
Pybik (Don't remember if installed)
GtkAtlantic
DOS software for DOSBox
Emulator Public Domain games

!![2019/02/15] - Repos where added later that may make it possible to add
                 some of these. Tor related packages where crossed-out in
                 the original notes because a disabled repo for them was
                 later added and an installer was added to the menu to
                 enable the tor repo and install some neat packages,
                 including grabbing the latest version of Tor Browser from
                 the website.
