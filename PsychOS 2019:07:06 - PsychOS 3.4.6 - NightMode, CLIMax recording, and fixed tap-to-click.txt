####################################################################################
# 2019/07/06 - PsychOS 3.4.6 - NightMode, CLIMax recording, and fixed tap-to-click # 
####################################################################################

To get TTY/console and just the terminal in general to use NightMode, I 
think I'll be using:

    NightMode --> echo -en "\\033[31m\\033[8]"
    DayMode   --> echo -en "\\033[37m\\033[8]"
    
I'll then have a hidden file created inside of "~/.CustomPsychOScripts"
folder that if contains "1" or "0" for on/off, I can use that with the
.bashrc file to have other programs like mocp change their theme based
on these values. MOC's themes are inside of "/usr/share/moc/themes" or
you can create a themes folder inside of "~/.moc".

Update [2019/08/23]: I may be changing "~/.CustomPsychOScripts" to
                     "~/.psychostools" sometime between now and the
                     next release.
                     
Turns out using the above mentioned idea (before the update section) isn't
going to work. And, I'm afraid that if I did figure-out how via the .bashrc
method that I would end-up messing things up. So, the current plan is to
have a list displayed after running nightmode in a command-line to show
what CLI programs have nightmode themes and how to use them.

I changed the quick audio note recording function in CLIMax to use OPUS.
It was using a 11025Hz WAV, but you can get the same quality for 1/10 the
size.

Added 'synclient tapbutton1=1' to 'Session and Startup'; this may hopefully
prevent any tap-to-click issues with touchpads, even though that the
problem was sort of solved (not paying attention) a while back.
