############################################################
# 2016/11/20 - PsychOS 2.6.1 - Guitarix and weather applet #
############################################################

*Add Guitarix presets under the name "PsychOS" if possible

*Had an idea to make an app that uses the waether applet to alert the user for agricultural purposes. May be able to use wxpython to make an interface for it. But, I'll still have to add it to the ./programs-list (climenu) eventually.

*Need to see if weather applet can be updated via the command line and then just echo the log for it.

*Replace Abstract.jpg in usr/share/wallpapers/xfce/ with newer one; increased contrast and it looks 100x better.
	*[Update 2017/03/03] - Turns out that it's a gamma issue. This will not be "fixed."

*Add Picard ID3 tagger to PsychOS
