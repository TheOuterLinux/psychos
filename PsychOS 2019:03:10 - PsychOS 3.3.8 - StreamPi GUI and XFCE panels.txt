#############################################################
# 2019/03/10 - PsychOS 3.3.8 - StreamPi GUI and XFCE panels #
#############################################################

Added/removed some minor packages and removed the CLI version of StreamPi for now
until I can finish StreamPi GUI, which seems as though a GAMBAS executable will be
a third of the size.

I also changed the XFCE panels to have six of them. One of them is the main panel(1)
that's always visible and the rest are hidden. Panel 2 contains the date/time,
weather, mail notifier, and power info (AC/battery). Panel 3 is for playing music with
DeaDBeeF and itmes below the controls are for launching other audio/video related
things. Panel 4 is located near the bottom left and only contains a workspace
switcher. Panel 5 is at the bottom right but is vertical and only contains Action
Buttons for screen locking and so forth. Panel 6 is at the bottom left, close to the
Whisker Menu icon and is meant for quick convenient things that has no business
being on the main panel(1).

However, placing these panels, even though most are hidden, causes new windows of
programs and file managers to open 2/3 -> top right. I need to at least see if it's
possible to open Thunar at the middle of the screen.

I wonderif it's possible to use 'tmux' for 'bristol' to temporarily open another panel
just long enough to run 'aconnect' and then close.

I may also add my "Math Station" 'tmux' thing I did for a Spooky Math Halloween video
tutorial.

I removed my Action Buttons panel because it kept getting in the way when scrolling.