#########################################
# 2019/01/13 - PsychOS 3.1.0 - Guitarix #
#########################################

Once again, 'Guitarix' fails to work. However, there are plugins for it in 'Audacity'.
This was made possible using 'guitarix-ladspa' and 'guitarix-lv2 packages.' The main
guitarix package was removed.

!![2019/02/15] - It was later found that the Audacity plugins work really well, but
                 do not work while playing live. This means you can have guitarix
                 sounds using PulseAudio/ALSA instead of JACK.
