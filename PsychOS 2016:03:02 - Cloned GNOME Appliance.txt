##############################################
# PsychOS 03/02/2016 - Cloned GNOME Appliance #
##############################################

* I tried to make some improvements based on another individual's 
  appliance from OpenSUSE Studio, but it didn't help. During boot, 
  PsychOS got stuck at trying to find a network. I may just burn a copy 
  of this individual's appliance and see if it works and then tweak it. 
  The owner has already stated his/her permission to do so since the 
  default GNOME appliance from OpenSUSE Studio seems to not have 
  everything it should. If successful, I will give him/her special thanks 
  for it.

* I should probably delete both PsychOS appliances and duplicate the 
  borrowed one before editing. I will say this though, for some reason, 
  the cloned "improved" GNOME appliance uses its own kernels. I have no 
  idea why, but I'll have to remove them since the appliance in question 
  is 64-bit when I'm trying to make a 32-bit OS for the time being.

Update:
---
* So far the cloned appliance works very well. It installed without any 
  issues. It connects to the Internet. However, htop needs to be 
  installed for better accuracy. top sucks.

* I'm installing packages over the cloned appliance and then plan on 
  exporting the list later.
    - Update:
        + Indicator plugin failed to install
        + XFCE mixer failed to install
