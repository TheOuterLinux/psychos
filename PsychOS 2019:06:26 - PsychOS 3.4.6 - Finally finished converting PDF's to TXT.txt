#########################################################################
# 2019/06/26 - PsychOS 3.4.6 - Finally finished converting PDF's to TXT #
#########################################################################

I finally got to finishing converting PsychOS Development Notes to all
plain text, However, I noticed a LOT of weird UTF-8 characters.

I had to run:

    find . -type f -exec sed -i 's/•/*/g' {} +
    
(You may not even see the above correctly)... and so on and so forth,
but that still left some UTF-8 characters and so I ran:

    find . -type f -exec iconv -f utf-8 -t ascii//translit {} -o {} \;
    
...I then needed to replace all "--" with single "-".

!! [Update: 2019/07/02] - Be very careful if using iconv and find together
                          because I think it may have screwed with the
                          file contents of the indexing .html files and
                          had to use a backup to restore those and then
                          add back the newer list items.
