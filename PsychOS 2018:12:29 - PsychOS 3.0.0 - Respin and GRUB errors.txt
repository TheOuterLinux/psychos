#######################################################
# 2018/12/29 - PsychOS 3.0.0 - Respin and GRUB errors #
#######################################################

*I tried using Respin, a Remastersys fork, but there are GRUB erros each time. If I try to install without GRUB, I get:
    errror: file '/grub/i386-pc/normal.mod' not found.
Using the boot from hardisk feature from the disc doesn't help either. I think I may have choosen the wrong option from Respin.

*I'm going to go ahead and give Devuan a try this time since when I had AntiX installed, it took forever to get to a point in which I could run any programs and I think it may have been because of Conky and because of the libre-kernel (4.19.x). If that doesn't work, then Debian Wheezy is next because I do remember using it with Remastersys successfully before the name PsychOS was even conceived.

Side-Note:
-----
If you're having dependency issues when running apt for no reason when installing packages from websites and the software appears to be working, you can can edit "/var/lib/dpkg/status" and find the associated package and remove items from the dependencies list.
