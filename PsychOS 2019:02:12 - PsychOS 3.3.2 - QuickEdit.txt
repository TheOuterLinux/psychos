##########################################
# 2019/02/12 - PsychOS 3.3.2 - QuickEdit #
##########################################

I spent most of the last few days working on 'CLIMax' and Thunar custom actions.
However, instead of having a bunch of menu items when right-clicking, I'm creating
a QuickEdit script and menu item instead. I'll still have the Thunar custom actions,
but I'll uncheck all of the filetytpes to hide them.