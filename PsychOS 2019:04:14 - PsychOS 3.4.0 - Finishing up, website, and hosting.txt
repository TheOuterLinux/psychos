###################################################################
# 2019/04/14 - PsychOS 3.4.0 - Finishing up, website, and hosting #
###################################################################

I'm pretty much done with PsychOS 3.x for public release and I think
StreamPi (GUI version) is finished as well. However, it's the website
now that is taking so long. I also don't know just yet where to host
PsychOS because of its large size. With StreamPi, you can either get it 
with PsychOS (tested and working) or grab it separately on the website.

I may not be able to use GitLab to host PsychOS, so...
    http;//linuxfreedom.com
    http://osdn.net
    https://sourceforge.net
    
    [Update: 2019/05/27] - GitLab will host large files such as PsychOS,
                           but you have to use git to upload to a repo
                           and there's a 10GB limit. Trying it via a web
                           browser will fail with a very small file size
                           limit.
