##########################################
# PsychOS 03/20/2016 - Ubuntu Remastering #
##########################################

* I've researched the use of Ubuntu-Builder, Remastersys, Ubuntu 
  Customization Kit (UCK), Respin, and Customizer (customizer-gui). 
  None of the Ubuntu builders work because all the builds, including 
  Ubuntu Minimal 12.04 return errors when trying to build off them.

* If an Ubuntu build were possible, the current plan would be to either 
  find a prebuilt distribution and customize it, or use the latest Ubuntu 
  Minimal LTS. Afterwards, Respin is the current tool of choice because 
  though it is terminal only (no GUI), it is very simple; Pinguy Builder
  is currently a no-go. However, I have no idea how to trick the build to
  rename the system to "PsychOS" as of yet.
    - The problem with Pinguy Builder is only the installer. And, the 
      latest version hasn't been used because it is my understanding that 
      the newer one only works for 15.04+ (when this note was created).
