##############################################
# 2019/04/15 - PsychOS 3.4.1 - Cleaning logs #
##############################################

I used 'echo "" > file.log' on a lot of log files in /var/logs/ to
free-up some space.
