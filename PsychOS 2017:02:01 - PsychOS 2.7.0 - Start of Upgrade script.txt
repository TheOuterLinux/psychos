########################################################
# 2017/02/01 - PsychOS 2.7.0 - Start of Upgrade script #
########################################################

*There will eventually be a release of PsychOS 2.7.0, but this will have to be in the form of an update script 
	and PsychOS 2.6.1 will officially be called PsychOS Core since all upgrades for now will use PsychOS 
	2.6.1 as a base.

*Need to remove and find replacements for OpenSUSE_games_13.2 and wine132

*|Packages to add to PsychOS Core:
  ----------------
	*kruler			*feh			*finch
	*extcalc		*filezilla		*python-MySQL-python
	*ipscan			*fslint			*python-PyMySQL
	*gfx			*acpi			*keepassx
	*dvdauthor07		*zenity			*fontforge
	*DVDStyler		*xsel

*|Packages to consider adding:
  --------------
	*wordgrinder
	*fontforge
	*Plotinus
	*Warpwallet
	*Tartini
	*music-visualization

*|Script options to consider:
  -------------
	*Frequency scaling
		- /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
			- change "ondemand" to "performance"
	*Add notitrans; don't forget chmod +x
		- Keyboard shortcut will need to be added by default after notitrans installation
	*Update OS information
	*[Update 2017/03/26]: Considering removing script step in Upgrade script; items mentioned in this 
		section were added to file corrections step.

*|File/package corrections:
  ------------
	*kruler icon and .desktop file
	*DVDStyler .desktop file (wrong category)
	*Update Lynis
	*Update VeraCrypt
