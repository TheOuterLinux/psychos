#######################################################################################
# PsychOS 09/01/2016 - PsychOS 2.5.6 - Not including LibreOffice templates by default #
#######################################################################################

* PsychOS 2.5.6 will not have LibreOffice templates installed with it by 
  default. I have tried to get it to work, but can't figure it out.
