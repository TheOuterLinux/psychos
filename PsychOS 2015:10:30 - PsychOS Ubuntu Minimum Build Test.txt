#########################################################
# PsychOS 10/30/2015 - PsychOS Ubuntu Minimum Build Test #
#########################################################

* Audio wouldn't work on Macbook Pro with Virtualbox.(See PsychOS 2016:02:18)

* Audio works fine on Acer Aspire One ZG5
	- Midori web browser audio from videos are a little low

* WACOM drawing tablet works

* iPhone is charging, though note that I'm jailbroken.

* Had htop, Midori with two videos fully loaded, Geary, and LibreOffice open with each in their own workspace and only using ~500ish MB of RAM.

* Volume changes relative to whether or not headphones are plugged in.

* Clementine is detecting iPhone and playing songs from it; I may need to be jailbroken for this.

*Shotwell detected photos and video from jailbroken iPhone

* Laptop charges jailbroken iPhone

* No lock screen shortcut yet.

* USB internet tethering from phone not working.
