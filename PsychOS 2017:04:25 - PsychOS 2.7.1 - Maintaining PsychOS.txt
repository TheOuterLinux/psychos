####################################################################
# 2017/04/25 - PsychOS 2.7.1 - Maintaining PsychOS (OpenSUSE 13.2) #
####################################################################

*Fixed a few things with Upgrade script like dialog stuff and removing xiphos because of dependency issues.

*I had the Upgrade script replace the OpenSUSE 13.2 repositories with Factory/Tumbleweed ones.

*Since I've started using Git, I've been laced on keeping notes; it may be due to coming towards just maintaining PsychOS.