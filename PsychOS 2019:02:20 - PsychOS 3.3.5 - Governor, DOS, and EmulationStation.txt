####################################################################
# 2019/02/20 - PsychOS 3.3.5 - Governor, DOS, and EmulationStation #
####################################################################

Added 'Gourmet' program for recipes, shopping lists, etc.

    [Update 2019/04/01 - This was later removed to make room]

Working on RecommendedSoftware (psych-rec) script.

Changed "/sys/devices/system/cpu/cpu*/cpufreq/scaling_governor" from "ondemand" to
"performance."

Need to add 'Mendeley' to RecommendedSoftware script. I would try to include 'zotero'
instead, but it seems as though Firefox is a dependency for some reason. I also
cannot include 'Mendeley' out-of-the-box with PsychOS because of its license.

I'm going to add DOS category and some public domain and FOSS programs. Because of
the way I have DOSBox's settings automount and etc. in the [autoexec] section, you
can't just click on a DOS EXE and run it. So at least for the Whisker Menu, I'll use
'dosbox -securemode -exit /path/to/file.exe' and will then also have to create a DOS
Category Editor. I could also add a Thunar custom action.

I'm trying to install EmulationStation from the RetroPi GitHub repo and if
successful, I may either remove the other emulators instead or add it to
RecommendedSoftware script. Either case, some symlinks need to be created so there
isn't a DOS folder and a seperate "pc" folder the EmulationStation uses.