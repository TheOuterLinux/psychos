#####################################################################
# 2019/09/05 - PsychOS 3.4.6 - Instant Meshes compile failed, a few #
#                              extra repositories added, and Jexer  #
#####################################################################

Trying to compile 'Instant Meshes' for a 32-bit system resulted in
freezing PsychOS and will not be included with the next release.

I went ahead and added a few repositories for Sid, RetroArch, and 
LibreOffice, but I'm only going to have the RetroArch PPA repository 
enabled out-of-the-box out of those three.

I was thinking about including a copy of 'Jexer', a TUI-like window
environment for the command-line, but I'll wait until 1.0 comes out and
the current version looks like an ASCII mess in TTY but fine in a
terminal emulator. https://jexer.sourceforge.io/
