###########################################
# 2017/06/18 - PsychOS 2.7.1 - PsychOS 64 #
###########################################

*I have been working on a 64-bit version of PsychOS and so far the ISO has been much smaller than I thought it would be. It is missing a few things compared to the 32-bit version but regardless, it shows how much space GNOME environment wastes.

*The trick right now is to get source code to compile because there's no way to get pip to run via the script on SuseStudio servers.
	*Homebank
	*Bristol
	*movgrab
	*mps-youtube
	*TermFeed
	*streamlink
	*termdown

*Homebank has already cause the build to fail because of missing dependencies. It's just the first one on the scripts list. This may be a challenge.