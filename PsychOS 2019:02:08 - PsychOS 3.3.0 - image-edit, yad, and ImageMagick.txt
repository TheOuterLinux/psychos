#################################################################
# 2019/02/08 - PsychOS 3.3.0 - image-edit, yad, and ImageMagick #
#################################################################

Speaking of 'image-edit,' 'dialog' looks like an ASCII mess in TTY and I have no
idea why that is.

Been looking around for interesting 'yad' scripts and I found some. I also found a
script that works with 'filebin' so I can give users a way to do easy 'Pastebin'-like
stuff but with more than just text files.

I worked a little more on my 'ImageMagick' TUI (Terminal User Interface). I also
'ln -s' 'ImageMagick' as "/usr/local/bin/imagemagick" because apparently, someone
thought 'display-img6.q16' was easier to remember.