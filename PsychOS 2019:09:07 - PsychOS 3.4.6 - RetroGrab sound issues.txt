#######################################################
# 2019/09/07 - PsychOS 3.4.6 - RetroGrab sound issues #
#######################################################

Yesterday I was working on RetroGrab and noticed that 'htop,' as well as
'pavucontrol' were reporting that a least the sound parts of RetroGrab
were still running even though the main parts of the program were not.
However, after running it again after reboot, it was doing fine and it 
could be a 'Gambas' specific bug instead. The Debian Stable (Stretch)
version of 'Gambas' are old, but I only plan to use what ever version
can be easily installed out-of-the-box.

[Update: 2019/10/20] - RetroGrab will not be using sounds as it causes
                       bugs and it just takes up too much space for
                       something not that important. Playing a dial-up
                       sound while retrieving bookmarks would be cool
                       but for now, oh well...
