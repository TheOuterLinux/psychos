###############################################
# PsychOS 02/14/2016 - Analysis of Puppy Linux #
###############################################

Applications
------
* Osmo - calendar, tasks, contacts, and notes
    - All-in-one program that's very lightweight
    - Uses only a few MB's of RAM
* ROX-File Manager
    - Very lightweight
* Inklight Vector Editor (Puppy only)
    - Would love to find an Ubuntu copy because it's an extremely lightweight version of Inkscape
* Viewnoir Image Viewer
    - Lightweight alternative to anything XFCE/GNOME uses
* Icon Finder by SFR 2012-2013
    - Looks through the whole system for icons
    - Great if you want to customize
* Gcolor2
    - Color picker; can save custom colors
* Markup Attribution Composition
    - Test markup text
    - Some websites still use this rather than have a fancy HTML 
      editor/WYSIWYG editor for posting
* HomeBank
    - Good for managing personal finances
    - Personal accounting
* Planner
    - A little more advanced task management than Osmo
    - Personal, client-side only CMS (Content Management System)
    - Manage personal projects
* gMeasures
    - Simple to use units converter
    - Weights and measures/temperatures
* Evince
    - PDF reader
* Peasy PDF Converter
    - Convert/join/extract PDFs
* Xcalc
    - Simple scientific calculator
* DidiWiki
    - Personal wiki editor
* Notecase Notes Manager
    - Place to keep password protected notes
    - Supports "nodes" and child-nodes; fancy lingo for subject and 
      sub-topics
* Bcrypt
    - Simple tool to encrypt/decrypt a file
    - Doesn't support folders
    - Drag and drop
* gFTP
    - FTP client for uploading/downloading files
    - Appears to be really easy to use like Cyberduck
* HexChat
    - Needs more investigation
    - May be possible to setup a free-node for other PsychOS users to 
      chat/share ideas
* Puppy Podcast Grabber
    - Stupidly simple podcast/RSS feed monitoring program
    - May only be available for Puppy Linux; need to find the source code
