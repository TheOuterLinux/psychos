############################################################
# PsychOS 04/11/2016 - PsychOS First Run and Things to Note #
############################################################

* At first boot after the installation of PsychOS takes about 30 seconds 
  extra to install configuration files. Afterwards, even on an older computer, 
  it should not take more than a minute to get from GRUB to beyond the 
  login screen.

Updating
----
* The first initial update of PsychOS with using the YaST Software Manager
  will ask you to trust sources that it doesn't recognize (GPG key issues); 
  this is because the repositories used with PsychOS come from OpenSUSE 
  Studio (www.susestudio.com). If you accidentally select not to trust a 
  repository, it will ask again the next time you use YaST Software Manager.

* The repository for Google Chrome will always fail because it is one of 
  the last 32-bit versions made. Google doesn't support 32-bit versions of 
  Chrome anymore. I would have included Chromium rather than Google Chrome,
  but Chromium does not support Netflix, which leads me to question what 
  other problems it might currently have performing. To remove this 
  annoying error, simply open YaST Software Manager and go to 
  Configuration -> Repositories... From there, select and delete the
  Google repository.
  
* In YaST Software Manager, all packages with upgrades available are 
  automatically marked. All you have to do is click "Apply" at the bottom 
  to update. Alternatively, you can run sudo zypper update in a terminal. 
  By accepting the repositories using YaST Software Manager, you can avoid 
  the annoyances regarding the repositories if updating with zypper.
  
The Top Panel (XFCE)
----------
* The first run of choosing to use XFCE from the login menu (the gear 
  symbol under the password entry) will ask you about what kind of panel 
  you would like to have. When in doubt, choose the default option. 
  This will create a panel at the top of the screen and a dock at the bottom.
  
* The first thing you may notice is that there is no volume indicator. 
  This can be fixed easily by right-clicking on the top panel and 
  Panel -> Add New Items... and then select "Audio Mixer."
  
* Items can be moved around by right-clicking on them and choosing "Move."

* It is recommended to add items such as Whisker Menu, Notes, Mail Watcher,
  Weather Update, Xfce4 Timer, and Battery Monitor.
    - The Whisker Menu is incredibly more useful than the default XFCE menu. 
      It has search options, favorites, recently used, and options typically 
      found within the Action Buttons (right corner username-looking thing).
    - Notes is useful for quick note taking, much like sticky notes but 
      on your system rather than surrounding your computer monitor with 
      them.
    - Mail Watcher will watch for new emails at a set interval. Mail Watcher
      has support for Remote IMAP, Remote POP3, Remote GMail, Local 
      Maildir Spool, Local Mbox spool, and Local MH mail folder, as well 
      as advanced settings for potentially other types of email watching. 
      A custom command can be added to run when new mail is found. 
          Example: 
              paplay /usr/share/sounds/KDE-Im-New-Mail.ogg 
      ...will play a sound when new mail comes in.
      
    - Weather Update checks the current weather as well as the forecast 
      for the current week. It's information is updated regularly using the 
      Internet. Location can be set automatically or manually. By default, 
      the weather is displayed in a scrollbox fashion. However, this can 
      be fixed by removing items under the "Labels to display" setting. 
      An icon representing the type of weather (sun, clouds, moon, etc) and 
      the temperature (Fahrenheit or Celsius) is displayed. It is recommended 
      to add more than one Weather Update to the panel if one travels 
      frequently.
    - Xfce4 Timer is a timer with options for custom names for timers, 
      a count down interval or a time to set for an alarm, as well as a 
      command to run at the alarm. This command can be anything, including 
      a sound or message.
    - The Battery Monitor is recommended because by default, there is no 
    icon that displays for battery information when not on AC power. 
    Information this plugin displays can also be customized.
    
* A potentially unfamiliar area of the panel is what appears to be four 
  boxes. Each of these boxes represents a "workspace." In other words, 
  four desktops to work with to help organize the clutter of windows on 
  your screen. I have noticed that using workspaces, as opposed to 
  minimizing an application, will save RAM. The package wmctrl is installed
  with PsychOS and can be used to control workspace switching from the 
  keyboard as follows: wmctrl -s 0. The "0" represents the initial workspace, 
  aka the first box. Keyboard shortcuts can be edited with Keyboard that 
  is found in the main XFCE menu under Settings.

* Another area of the XFCE panel is the Indicator area. This is the area 
  in which items such as Bluetooth and WiFi Internet show up. This is 
  because not all devices, contrary to popular belief, have Bluetooth or 
  Internet capability.
  
* The last area in the right corner of the panel will display the user 
  name that is currently logged in. Clicking on this will invoke options 
  regarding screen-locking logging out, suspending, hibernating, and so 
  forth. Such options are also located within the Whisker Menu. I recommend 
  ticking-off the save for future sessions option; if the reason you are
  restarting is system issues, it may show up again after login.
  
* The default time is set to a 24-hour format. This can be changed, as 
  well as add code for displaying the month, day, and year. Clicking on 
  the time will quickly popup Orage calendar. Click the time again to 
  make it go away.

The Bottom Panel/Dock XFCE
-------------
* To add an application, right-click the dock and select to add a new item 
  to the panel. From there, choose "Launcher." A blank item will be added 
  to the dock. Right-click on the blank item and go to "Properties." Click 
  the plus symbol and type in the name of the application in the search bar. 
  You can also click the new document looking symbol for more application 
  options such as terminal only or to add standalone applications downloaded 
  from the Internet.

* You can change the icons used in the dock via Properties per item or via 
  Settings in the XFCE menu and selecting Appearance. I recommend using 
  Numix under "Style" and Faba under "Icons;" this combo seems to get 
  the most bang for your buck without hogging too much RAM. However, Moka
  is my favorite icon set.
  
* Multiple items can be added to each "Launcher." This could be useful 
  for categorical reasons.
  
* It would be best to go ahead and change the Internet icon to launch 
  Midori and then select Firefox or Chrome when needed from the 
  XFCE/Whisker Menu. This will save you much time and headache if just a
  quick Internet search is all you need to do.
  
* I would also go ahead and add Thunderbird, LibreOffice, and the Trash 
  Applet to the dock.

* The search applications launcher is changed to open catfish file search 
  on my system.
  
* The bottom dock can be moved to the sides of the screen, as well as 
  automatically show/hide.
