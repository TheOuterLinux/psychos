#############################################################################
# 2019/02/06 - PsychOS 3.2.9 - Thunar custom actions and package management #
#############################################################################

I spent all day yesterday adding Thunar custom actions for converting files and only
managed to get through image and document formats. I still have to add options for
audio and video. I also added a few conversion packages in the process.

I added 'pacapt' and created an alias in the "~/.bashrc" file so you could use
"pacman" instead. 'pacapt' is just a 'pacman' emulator that actually uses 'apt' and
'dpkg'.

I found an interesting but kind of useless program called 'Wet Dream' in which the
author had intentions of turning it into a 'GIMP' addon. It's a tiny water-color
painting simulator program.

I also added 'gtkmorph' because why not? 'GIMP', or at least what comes with PsychOS,
has morphology tools in the video section, but sometimes it's nice to have an
application that does one thing really well. But why morphology? Blender. I can
export a blank UV Map (with grids) of a human 3D model and set points of a person's
face and UV Map to match and then do some tweaking to make adding faces to 3D models
easier.

I'm going to look into using https for package updates instead of plain http. I may
also look into using apt-fast to make package downloads faster. I wonder if I can
use 'axel' with this? If so, I'll probably go no higher than 'axel -n 10.'
Nevermind; it turns out that 'apt-fast' already uses 'aria2' for multiple mirror
downloads.

By the way, the reflection in the parking garage wallpaper for PsychOS is wrong
and needs to be mirrored correctly.

I wasn't able to get all of the repos to use https because of SSL errors. Also,
you have to run 'apt-fast' as sudo, so I'm not making an alias for it. I found
a neat program/script called 'netselect-apt' for finding faster package mirrors.

I tried to add 'zypper,' but it looks like that might be a lot of work for
something that could mess-up PsychOS. I did however get yum on there. I have no
idea if it really works because I don't have any repos for it.

Installed 'Anki', a flashcard/memory program.