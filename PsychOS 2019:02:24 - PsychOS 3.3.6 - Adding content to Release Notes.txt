################################################################
# 2019/02/24 - PsychOS 3.3.6 - Adding content to Release Notes #
################################################################

I had to restart with a fresh copy of PsychOS 3.3.6 because of all of the
Git/symbolic link nonsense I tried.

Added more to my Release Notes. Added several file formats, but I need to just open
all of my programs on a different computer and see what file formats I can add to
the list. Opening them on the PsychOS production computer would just create a bunch
of hidden settings files.

Added 'G3DViewer'

I added a lot more file formats to the Release list and gearing toward adding 3D
model support to QuickEdit.