##############################################################################################################
# PsychOS 08/03/2016 - PsychOS 2.5.5-2 - Added things, fixed things, and PsychOS development notes on GitHub #
##############################################################################################################

What's new:
-----
* Fixes were made for Mendeley  and Rstudio
    - Alien was used to convert the Mendeley  .deb package to .rpm.
    - Alien was used to convert R Studio .deb package to .rpm and then 
      the system installed the dependencies on its own.

* Git tools were added

*  PsychOS development notes were uploaded a few days ago and will 
   continued to be uploaded to Github.
   
Issues that still need fixing:
---------------
* MIDI controller/keyboard not being picked up by JACK

* ZynAddSubFX  typically has both a JACK and ALSA version installed. 
  The ALSA version is missing and probably will remain that way.
    - EDIT: Just realized that ZynAddSubFX  via JACK works on a previous 
      build of PsychOS (2.5.4) . Though, this could be because of one of 
      many added packages while testing PsychOS 2.5.4. ZynAddSubFX  was 
      not added by default. It, along with many other audio apps, were 
      added after installation.
    - Added Qsynth and FluidSynth  to perhaps add libraries to fix this.

* GNOME-maps  was fixed by GNOME; however, OpenSuSE hasn't uploaded the 
  current version to susestudio.com for 13.2 (32-bit). This is out of my 
  control.
  
* Octave still only has a gui if initiated through the terminal. No idea 
  why they designed it that way. Use "octave -force-gui ."

* Renaming the ISO after download to "2.5.5-2" to reflect the changes 
  that were originally meant to be fixed while still adding and removing 
  a few packages here and there for an overall improvement.
    - SUSEStudio does not allow "-2" in naming builds.

* Before releasing PsychOS 2.5.5-2, I may go ahead and try to see if I can 
  run the Netbeans installation shell script on susestudio.com; if not, 
  I'll try Eclipse.
    - PsychOS has Geany installed, but I am worried that it isn't 
      advanced enough for a large project. However, Geany isn't leaving 
      PsychOS anytime soon.
        [] Update [2016:08:04]: SUSEStudio seems to have a very limited 
           custom script runner; Netbeans  cannot currently be installed 
           via this method. Repositories were found for Netbeans, but 
           SUSEStudio limits 30 repositories per appliance. I would like 
           to have Netbeans  installed, but it isn't important enough to 
           warrant the replacement of a used repository.

21 new dependencies:
----------
* cvs
* cvsps
* fluidsynth
* giggle
* git
* git-core
* git-cvs
* git-doc
* git-email
* git-gui
* git-svn
* git-web
* gitk
* libserf-1-1
* perl-DBD-SQLite
* perl-DBI
* perl-Error
* perl-Term-ReadKey
* qsynth
* subversion
* Subversion-perl

12 new software selections:
-------------
* libQtWebKit4
* git
* giggle
* git-gui
* gitk
* git-email
* git-svn
* git-web
* git-cvs
* git-doc
* qsynth
* fluidsynth

2 software selection removed:
--------------
* R-rstudioapi
* retroarch
