####################################################################################
# 2019/01/17 - PsychOS 3.1.1/2 - grub-imageboot, fdutils, and fork-bomb prevention #
####################################################################################

I need to find an easy way to change the date/time

Custom ".desktop" files inside of $HOME may need to be moved to "/".

I wonder if there's a way to prevent BASH fork-bombs like :(){:|:&};:

Add diffuse and bum

I may place process number limits to prevent fork-bombs, but I don't
know if that will make people angry or not. I'm already pushing it
with all of the other security stuff.

With tweaking, current boot to autologin to the point of being able to 
run applications is 2 minutes.

Adding floppy tools: fdutils ufiformat

Installed 'grub-imageboot' to help booting FreeDOS live ISO. Then, all I
had to do was move the ISO to "/boot/images/" and run 'sudo update-grub'.
The order of the files in "/etc/grub.d/" determines there menu items show.
I renamed the image related one from "60_*" to "15_*".

I need to rename the Devuan parts of the GRUB menu and change the icon
and logo images.

Planning to add KolibriOS and need to look at AROS license, but probably
not due to size.

    !![2019/02/17] - AROS license doesn't appear to allow this and the
                     ISO size is too large, as well as most related
                     projects.
