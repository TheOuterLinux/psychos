############################################################################
# PsychOS 08/23/2016 - PsychOS 2.5.6 - Panel, menu, and keyboard shortcuts #
############################################################################

* Top panel needs to be larger (40px)

* Menu items still missing icons; may have to point with entire address
    - Edit [See PsychOS 2016/08/24]: Fixed via correct image file
      placement
      
* Keyboard shortcuts don't work in new users should XFCE, only root.
    - Edit [See PsychOS 2016/08/24]: Works in IceWM
