#######################################################
# 2017/03/25 - PsychOS 2.7.0 - Replaced nano with jed #
#######################################################

*Look at for labels and tags to file:
	*https://github.com/cytopia/lsl
	*tmsu

*Need to remove Lynis's "-More-" thing in the terminal

*Replace bcrypt in accessories-list with PGP

*Check nano for file placement and autosave files in terminal folder

*Check jed for some issues as nano

*Zork game has issues

*View image file default start location needs to be changed
*Some for PDF's and cancel button is missing

*Eliminate the "e" in "video" in multimedia-list for search YouTube

*Turns out MOC can still play music even after "quitting"

*I may want to change the file select box for saving a Lyx template in /tmp/ to an input box or make it so it only stores the filename and extension
