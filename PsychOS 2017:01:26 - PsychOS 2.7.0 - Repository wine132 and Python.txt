##############################################################
# 2017/01/26 - PsychOS 2.7.0 - Repository wine132 and Python #
##############################################################
*Updated PsychOS 2.6.1 and working
	- OpenSUSE_games_13.2: http://download.opensuse.org/repositories/games/opensuse_13.2/Emulators/
	openSUSE_13.2/wine132
*Need to add package python-MySQL-python and python-PyMySQL. This will help with any future applications I might 
	make that need a database.
*Need to install KeePassX because it's not just for saving passwords. You can also use it to save web links, 
	notes, and file attachments and even supports key file for a two-step encryption-like thing if wanted.
*Need to add something like PyGTK or wxPython for building GUI programs.
