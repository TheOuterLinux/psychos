#################################################################
# 2017/02/27 - PsychOS 2.7.0 - Batch image editing for contrast #
#################################################################

*Batch Contrast Change
	*Options:
		-Pick folder
		-Recursive or non-recursive
		-Increase or decrease contrast
	*Order:
		-Increase or decrease contrast
		-Pick folder
		-Recursive option
	*Variables
		-contrast (+, -, increase, Increase, decrease, Decrease, q)
		-type (jpg, png, bmp, tiff, *, q)
		-location (/folder/)
		-recursive (y/N/q)

*Place "if/then" statements throughout for options to quit (q)
*Echo the files to be changed at the end of recursive option and ask for confirmation ($confirmation)

#Need to double-check .image-edit

A ridiculously rough and incomplete draft:

	echo -n "Increase or decrease contrast (+/-/q)?: "
	read contrast
	# dialog file picker reads as $location
	echo -n "What filetypes? (jpg, png, bmp, tiff, *, q)" # Forgot gif; added later
	read type
	echo "You've selected folder $location with filetype $type."
	echo "If you edit recursively, the filetype $type will also be edited in the following folders."
	echo -n "Edit recursively? (y/N/q): "
	read recursive
	
*[Update: 2017/03/29] - Most of the items such as "echo" and "read" are being replaced with "dialog."
