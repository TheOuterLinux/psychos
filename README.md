#PsychOS
[https://psychoslinux.gitlab.io](https://psychoslinux.gitlab.io)

##Code named "Insanity"


- [Click here to try it online!](https://distrotest.net/PsychOS/3.4.6)
- [Click here to Download](https://gitlab.com/PsychOSLinux/distributions/raw/master/PsychOS_3.4.6.iso)
- [Click here to view respin source](https://gitlab.com/PsychOSLinux/distrosource)

    - **Default username:** psychos
    - **Default password (login and root):** linux
    - **Initial release date:** December 25, 2019
    - **Version:** 3.4.6
    - **Target:** Older i686 computers with DVD-ROM drives
    - **Bare minimum RAM needed for booting Live ISO:** 220MB
    - **Okay RAM minimum:** 512MB
    - **Happier RAM minimum:** 1GB
    - **Download Size:** 3.8GB
    - **Installation Size:** ~11GB on EXT4 file-system
    - **Supports persistence:** No
    - **MD5:** 57bc4d9af162ce1aecb35f30f9f8f07a
    - **License:** [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)
    - [Software installed using apt](https://psychoslinux.gitlab.io/documents/AptInstalledList.txt)
    - [Software installed using pip](https://psychoslinux.gitlab.io/documents/PipInstalledList.txt)
    - [Manually installed software](https://psychoslinux.gitlab.io/downloads.html#OtherResources)
    - [USB Booting Instructions](https://psychoslinux.gitlab.io/downloads.html#usb)

---
PsychOS is a [systemd-free](https://nosystemd.org/), [GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.en.html) operating system based on [Devuan ASCII](http://files.devuan.org/devuan_ascii/) but tailored towards [retrophiles](https://en.wiktionary.org/wiki/retrophile) of all ages. It is packed with software and scripts for an easier, out-of-the-box user experience.

###This 3.8GB ISO Includes:

- [3000+ installed packages](https://psychoslinux.gitlab.io/documents/AptInstalledList.txt)
- [90+ Python modules](https://psychoslinux.gitlab.io/documents/PipInstalledList.txt)
- [390+ Supported file formats](https://psychoslinux.gitlab.io/documents/PsychOSReleaseNotes.txt)
- [Interesting Thunar Custom Actions](https://psychoslinux.gitlab.io/documents/Thunar/DescendingIndex.html)
- [Customized .bashrc](https://psychoslinux.gitlab.io/documents/bashrc.txt) for quickly doing things in the command-line for when [CLIMax](https://vimeo.com/459432556) isn't enough
- [DOS software included](https://psychoslinux.gitlab.io/downloads.html#OtherResources) with [RetroGrab](https://vimeo.com/461533568) to add more

For those wanting to review PsychOS, please [read this](https://psychoslinux.gitlab.io/review.html) first.
