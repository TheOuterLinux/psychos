###############################################################
# 2016/11/24 - PsychOS 2.6.1 - climenu and WaveSurfer removal #
###############################################################

*Add plugin/module to mc to store files/folders as variables.
	*[Update 2017/03/03] - It doesn't look like this is going to happen. However, I may be able to create
	a recent default folder for other programs like the image editing sections of the climenu.

*Add fdupes to look for and delete duplicate files

*NeoFetch for system information and custom ascii art

*Check to see what calendar cli is installed, and see about setting up a cron job for it to sync calendars, if it'll read .ics.
	*[Update 2017/03/03] - This may not ever be possible because many cloud services are revoking 3rd party
	software for security reasons.

!!!Need to seriously look into WaveSurfer playback error
	*"Error: Could not gain access to /dev/sound/dsp for writing."
	#I might actually just get rid of WaveSurfer since after using it a few times, I don't understand what it
	actually does; Audacity seems to already have the same tools.
		*Audacity actually does have analysis tools with expert options I could use to get into a
		spreadsheet and then PSPP for statistics.

*Check to see if dialog can display files in a folder. The ability to scroll and select would be awesome.
	[Update 2017/03/03] - you can:
		*location=$(dialog -stdout -no-cancel -backtitle "$BACKTITLE -> Blur Image" -title "Please
		choose an image to blur:" -fselect $HOME/ 10 75)

*Need to fine reference apps or add formulas to ./programs-list

*See about cutting off http and https in Firewall off by default
	[Update 2017/03/03] - Looking back at this, I have no idea what I was thinking.
