#######################################################
# 2017/05/09 - PsychOS 2.7.1 - Audio analysis and EEG #
#######################################################

*Looked at some of the audio analysis software that uses Python; most of them require the internet and an API key.

*Need to take a look at an EEG program called TEMPO that uses EDF format and Qt to create animated topographical maps of brain activity.