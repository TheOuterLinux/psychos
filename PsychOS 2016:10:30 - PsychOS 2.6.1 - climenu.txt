########################################
# 2016/10/30 - PsychOS 2.6.1 - climenu #
########################################

*Install fortune and cowsay

*Install TermFeed via sudo pip install TermFeed

*Add File Manager, vlock, systemctl suspend, etc. to the main menu, or maybe the "System" part of the command line menu project.

*Add liblo0-0 to repo for Qtractor if user wants to install it.
