####################################################################
# 2019/05/05 - PsychOS 3.4.5 - Pale Moon redistribution correction #
####################################################################

Ran into Pale Moon's Redistribution License on accident and apparently,
I'm going to have to wipe out all profile/settings as if never ran and
then a have separate folder with the addons I wanted to include and let
users decide whether or not to install them.
