######################################################################
# 2017/04/14 - PsychOS 2.7.1 - python-traitlets-4.3.2.7.1.noarch.rpm #
######################################################################

*Ran into a possible security issue with PsychOS today; python-traitlets-4.3.2.7.1.noarch.rpm had the wrong checksum, according to zypper. I skipped over its update.
