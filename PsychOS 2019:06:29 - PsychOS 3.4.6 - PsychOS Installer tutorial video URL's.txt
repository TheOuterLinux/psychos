#######################################################################
# 2019/06/29 - PsychOS 3.4.6 - PsychOS Installer tutorial video URL's #
#######################################################################

Worked some more on the PsychOS Installer and started adding "?" buttons
that would open a media player (gb.media) and play a YouTube video.
However, I also had the idea to have curl or something similar (wget?)
to grab a URL from a file online instead so that I can change that video
any time I need to. This technique may also benefit any future net
installers.
