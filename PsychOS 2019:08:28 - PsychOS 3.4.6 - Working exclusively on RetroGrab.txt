##################################################################
# 2019/08/28 - PsychOS 3.4.6 -  Working exclusively on RetroGrab #
##################################################################

I've been working exclusively on RetroGrab and when finished, PsychOS
3.4.6 should be ready.

In the Graphics section of CLIMax, catimg is used as an ASCII image
viewer. I may be replacing it with chafa as the images are much sharper 
and it has animated GIF support just like catimg. It also doesn't
freak-out if the file extension of an image is something like "*.bmp3".
