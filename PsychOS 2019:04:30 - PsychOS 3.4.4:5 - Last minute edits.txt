####################################################
# 2019/04/30 - PsychOS 3.4.4-5 - Last-minute edits #
####################################################

I'm trying PsychOS 3 finally on a a 64-bit machine using a live DVD. At
login, RAM usage is ~230MB and it has access to all the RAM because of
the PAE kernel.

There was one very weird thing that was concerning me and that was Palemoon's
Preferences menu not switching visually between the tabs (General, Tabs,
Context, etc.). However, it's working just fine on another laptop, so
maybe it only shows-up after installing. Before I check this, I'm going 
to run a bunch of things Live while I'm at it first.

    [Update 2019/05/28] - Nevermind; mothing to worry about at all
                          because no one is allowed to preset Palemoon
                          anyway, according to their redistribution
                          license: https://www.palemoon.org/redist.shtml
                          
Forgot to add /home folder items to applications menu.

LMMS can convert a project file from command-line; I may add this to
QuickEdit. I did make SDL the default AUDIO INTERFACE for LMMS.

The Bristol Synths CLI needs some tweaking. I'm not sure if the MIDI
keyboard variable is being stored correctly.

On StreamPi, the default scale was set to 600 on the 64-bit computer, and
I'm not sure why.

I'm removing the Recommended Software menu item because of the website.

I was having mpv/youtube-dl issues. Running 'sudo pip youtube-dl -U'
fixed it. Outdated token? I should probably update all pip installed
packages.

Just realized something; I'm going to have to create a separate site for
projects like PsychOS and StreamPi because the current URL's are too
long. I don't need to make full-fledged pages, just redirects to the
TheOuterLinux version.
