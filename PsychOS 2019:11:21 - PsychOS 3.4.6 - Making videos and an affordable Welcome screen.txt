###############################################################################
# 2019/11/21 - PsychOS 3.4.6 - Making videos and an affordable Welcome screen #
###############################################################################

I think PsychOS 3.4.6 is almost finished as there is not much left
(famous last words) to add or improve. I've spent most of the time since
the last journal entry adding videos to BitChute and PeerTube while
doubling as debugging because when making a video on something you've
made, what can go wrong usually does. I've also added more features and
stability to 'QuickEdit'.

I was thinking about having a Welcome screen like what many GNU/Linux
distributions do, but that realistically only serves to slow down login
time. Does anyone actually use it? However, I'm not a complete moron, so
I made a "cheap" compromise by having the 'xfce4-notes' display a quick
message at login with also how to get rid of it.

As for creating any bug reporting goes, maybe in the next release or two
but honestly, users should just get on PsychOS's GitLab or BitBucket
repository and repost issues there, preferably GitLab.

[Update 2020/01/05]: There is link on https://psychoslinux.gitlab.io
                     for reporting issues.
                     
There still are a few more videos to make and some things to check on
while running PsychOS live and after an install, such as making sure all
necessary settings transfer to a different username.
