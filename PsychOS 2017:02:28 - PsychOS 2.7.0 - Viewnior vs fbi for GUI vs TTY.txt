###############################################################
# 2017/02/28 - PsychOS 2.7.0 - Viewnior vs fbi for GUI vs TTY #
###############################################################

*Lately I've been working on the Graphics section of climenu

*I've figured out how to distinguish between normal terminal (pts) and tty

	TTY=$(tty)
	TMP=${TTY%/*}
	TMP=${TMP##*/}
	if ["$TMP" == "pts"]
	then
		...
	else
		...
	fi

*Also, figured out how to do a slide-show with fbi.
	sudo fbi -noverbose -a -u -t 6 *
		*You have to cd into the directory for it to work

*Using slide-shows and image viewers to make sure correct image is selected, folder is selected, and show an 
	"after" when done.

*I've made it to the "Despeckle" sections of both "image" and "batch."

#May be able to use pv utility to show a progress bar and an eta for the while loops used somehow

	sudo pv -tpreb /path/to/source | ...
		*Need to do more research
