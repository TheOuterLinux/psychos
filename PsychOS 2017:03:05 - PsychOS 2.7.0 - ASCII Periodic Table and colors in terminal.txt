############################################################################
# 2017/03/05 - PsychOS 2.7.0 - ASCII Periodic Table and colors in terminal #
############################################################################

*Worked on Graphics section of climenu; I'm almost done except I know I'm going to have to go back and add error 
	checking to some of the sections.

*Added an ASCII periodic table to the gnuperiodic part of Education section. I've also included colors for 
	sectioning the different types of elements.
		- example: NC = "\033[0m"
			   RED = "\033[0;31m"
			   ${RED}word${NC}
		- ${NC} acts as a color killswitch
