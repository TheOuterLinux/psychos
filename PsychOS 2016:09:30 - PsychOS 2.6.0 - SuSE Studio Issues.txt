###########################################################
# PsychOS 09/30/2016 - PsychOS 2.6.0 - SuSE Studio Issues #
###########################################################

* There's something horribly wrong with susestudio; I can't really do anything at the
moment unless I try building locally.I emailed them about the issues. Basically,
builds fail within 30 seconds. I even tried making a 64-bit version (42.1
Tumbleweed) and it randomly wipes itself clean of everything, including
repositories.

* As soon as I can, I need to gather up all the added files, including RPMs and save
them somewhere just in case the servers start having even more trouble.

* !!! Update [10/01/2016]: I synced each repository under the software tab in
susestudio and even though a few of them failed to sync, it still fixed the issue of
building. The 64-bit I put together is still messed up.
