#######################################################
# PsychOS 10/02/2015 - Trisquel OS vs. Tiny Core Linux #
#######################################################

Tiny Core Linux
--------
	* Core Plus version comes with a remastering tool
	* Total size is 86 MB
	* Ezremaster
	* Don't forget burning tool (Brasero or K3B)
	* Desktop backgrounds are saved in /opt/backgrounds

** Remastersys

Trisquel OS
------
	* Do not download the Sugar version; too confusing to use.

	** Would like to try Tiny Core, but I'm not sure about it's repo.
	* Repos limited to Tiny Core's own stuff.
		*No .deb or .rpm

	** Need to check out DSL

	** Trisquel doesn't recognize WiFi "out-of-the-box."

Not Authenticated
---------
	* kodi
	* xbmc
	* intel-linux-graphics-installer
	* libtag1-vanilla
	* i965-va-driver
	* libtag1c2a
	* intel-gpu-tools
