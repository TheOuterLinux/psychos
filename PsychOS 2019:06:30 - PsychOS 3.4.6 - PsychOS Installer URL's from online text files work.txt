#########################################################################
# 2019/06/30 - PsychOS 3.4.6 - PsychOS Installer URL's from online text # 
#                              files work                               #
#########################################################################

The idea to pipe values from text files online to variables in the
installer worked. Now, I just need to figure out how far I want to go
with this idea. I'll also probably make files specifically designed to
compare with each other so if they don't match, like if an account got
hacked, it won't play the video at all; having porn played on an installer
for a Linux distro would be bad. But, this also means that the installer
itself cannot be included in the same repository as the text files. I may
not even release it outside of the distribution anyway. 
