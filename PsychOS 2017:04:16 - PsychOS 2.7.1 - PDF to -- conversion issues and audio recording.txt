################################################################################
# 2017/04/16 - PsychOS 2.7.1 - PDF to - conversion issues and audio recording #
################################################################################

*I've been working on ./office-list, single and batch file conversion, and the PDF to - is giving me hell.

*./multimedia-list now has audio recording that works, but I'll still have to go back and add more menu options for recording devices, audio effects (SoX), etc.. The recording is being done with ffmpeg because it supports "q" to quit.
	[Update 2017/04/20]: SoX will not be used at all; it doesn't work on PsychOS.

	Rough draft (brain-storm) of audio recording menu:
		1. <- Go Back
		2. New Recording
		3. Edit Recording
			1. Convert
			2. Trim
			3. Amplify
			4. Reverb
