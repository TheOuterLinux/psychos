####################################################################################################
# PsychOS 06/29/2016 - PsychOS 2.5.3 - OpenSuSE branding packages are ugly and added more software #
####################################################################################################

* PsychOS 2.5.2 was created on June 27, 2016; however, I forgot to remove 
  the OpenSUSE branding packages that quite frankly make the whole system 
  look hideous.

* PsychOS 2.5.3 was created on June 28, 2016.
    - The program RStudio was added, but will need to be removed because 
      there are binaries missing.
    - I installed Octave, but it has to be run through the terminal. 
      Octave-gui is installed, but I guess it only shows up when running 
      a compatible file.
    - QJackCTL was added, along with other audio applications such as 
      ZynAddSubFX (MIDI and ALSA virtual synth piano), Guitarix 
      (use computer as preamp), and LMMS (a multimedia studio that uses 
      WINE); this consequently installs WINE, killing two birds with one 
      stone.
    - WINE is added in order to help Linux newbies run simple Windows
      applications but with a very miniscule chance of getting a virus; it 
      would only affect the Windows libraries and applications and shouldn't 
      transpose to a Linux application unless piped somehow like via 
      "pipelight."
    - OpenLP, a program technically designed for church presentations and song 
      word display, was added to PsychOS because you can import your own 
      presentations and there are apps for both Android and iOS for remote 
      control. LibreOffice has them too, but its just in case there are 
      issues.
    - OCR software was added to read words off of scanned documents.
    - Xournal was added for handwriting notes and to annotate PDFs.
    - Google Chrome was abandoned due to lack of 32-bit support; Chromium
      is its replacement.
        [] The packages chromium-pepperflash-plugin and chromium-widevine-plugin
           were added for better DRM compatibility.
        [] Plays Netflix just fine.
    - GCC compiler packages were added.
    - FLTK development package was added to make installing Dillo easier for
      when it will be mentioned in PsychOS help guide. I really wish they'd 
      make an easy to install binary.
      
* I wanted to add NetBeans, but the installer is a shell script. SUSE Studio
  is capable of running scripts during a build, but I'm not sure if it 
  would run the Netbeans installer or not and actually work. I wish I 
  could find an example similar to this. I would really love to be able 
  to compile from source code with SUSE Studio; I could get a LOT more 
  wanted applications installed.
