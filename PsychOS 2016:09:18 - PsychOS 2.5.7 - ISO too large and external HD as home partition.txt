########################################################################################
# PsychOS 09/18/2016 - PsychOS 2.5.7 - ISO too large and external HD as home partition #
########################################################################################

* The susestudio.com system keeps failing; the ISO may be too large. I 
  have noticed that modifying packages and then building sometimes helps.
  
* Tried Marble; it's way too buggy and different for practical use.

* Using an external HD as /home is possible but not recommended.
    - Causes errors with installed software and file manager eventually;
      probably momentary connectivity loss
      
* Testing something interesting while I have the opportunity; currently using an
  external hard drive as /home partition (second half). I unplugged it, ergo no
  /home so I'm curious as to how PsychOS would react and what applications will
  still run. Be aware that hard drive on the computer itself is split as:
    - /boot
    - /
    - swap
    - /tmp <- normally /home
    
* I may try to initiate a build of PsychOS using a different computer, 
  even though it's server-side.
    - Cloning it may help too
    - This is only because builds are failing repeatedly and may be due to
      PsychOS's large size.
      
* I unplugged the external hard drive with the /home partition (encrypted) 
  and tested a whole bunch of applications to see what would run. Turns 
  out that most will run just fine except those that require Internet 
  and Bluetooth.
  
* cdw was installed for easy disc burning via a command-line

* Installed mpg123 for playing mpeg-1, 2, or 3 files via command-line

* Installed mc (midnight commander) for file browsing via command-line
