#################################################################
# PsychOS 12/24/2015 - Thunar vs. Nautilus and Large ISO Build #
#################################################################

* ISO size is currently 4.1 GB in size

* Switched from Thunar file manager to Nautilus file manager in order to 
  use Gloobus-preview (Quick-look clone).

* A Synaptic Package Manager backup was created for manually installed 
  programs

Youtube
----
* Plays HD videos like a boss
* If paused, the video may lose audio sync on resume
    - I think it might be random
* No Netflix support without Chrome, and Chrome crashes the system.
    - Running Chrome on OpenSUSE does not crash the system. Last tested
      sometime around March 8, 2016
