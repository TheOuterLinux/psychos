##############################################################################################
# 2019/12/21 - PsychOS 3.4.6 - Website, better updating script, and a Whisker Menu discovery #
##############################################################################################

I'm very close to finishing PsychOS 3.4.6 and so far the things slowing
me down are developing the website for it and the few little issues I have 
found while running it live.

In the PsychOS Tools "set," I have removed the 'psychos-update' script and 
corresponding files and instead, I am placing a 'psychos-update' script
in "/usr/local/bin". This script will show a TUI menu with a few options
such as "Upgrade PsychOS Tools," "Upgrade all apt installed software," and
"Upgrade all pip installed Python modules." Before, the 'psychos-upgrade'
script was trying to do a whole lot in one go and it probably would have
made people angry and concerned as it could have been used to install
extra software (if I wanted) and it's security depended on GitLab or 
BitBucket (used first available repo).

'RetroGrab' and 'QuickEdit' are pretty much finished but more TTY testing
is needed for QuickEdit.

I discovered something interesting on regards to searching for installed
applications. For whatever reason, the Whisker Menu only shows results
based on the program name and the description but not the keywords within
a .desktop file. However, it seems as though .desktop files except "#"
within the "Exec=" as notes the same way BASH scripts do and "Exec=" is
part of the search results. So instead of adding keywords, add "#tag1,
tag2, and so forth" to the end of whatever is inside of the "Exec=".
