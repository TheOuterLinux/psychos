####################################################
# 2019/03/17 - PsychOS 3.3.8 - StreamPi and FFMPEG #
####################################################

Just to update, I've been working on StreamPi. I've got quite a bit of it done.

It seems as though FFMPEG will convert FLI files but not to them. I also need to add
GIF87 convert option to "~/.bashrc" and to QuickEdit as well as BMP 1, 2, and 3.

It also wouldn't hurt to run 'ffmpeg -formats' to get a demuxing/muxing list for
adding extensions to QuickEdit and the formats supported list for the Release notes.

    DE and E go on the normal FFMPEGLIST
    D goes on the other list